import { kv } from '@vercel/kv'
import { OpenAIStream, StreamingTextResponse } from 'ai'
import { Configuration, OpenAIApi } from 'openai-edge'

// import { auth } from '@/auth'
import { nanoid } from '@/lib/utils'

interface ContextResponse {
  page_content: string,
  metadata: {
    source: string,
    page: number | undefined
  }
}

export const runtime = 'edge'

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY
})

const openai = new OpenAIApi(configuration)

export async function POST(req: Request) {
  const json = await req.json()
  let { messages, previewToken, selectedSource } = json
  // const session = await auth()
  const question = messages[messages.length - 1].content;
  // if (session == null) {
  //   return new Response('Unauthorized', {
  //     status: 401
  //   })
  // }

  // Context Gathering
  // console.log(messages)

  // pinecone_small, zilliz_small
  // latest zilliz_small
  // http://3.101.85.74:8001/get_context?message=

  try {
    // Все документы
    // Уголовный Кодекс
    // - Конституция РК
    // - Документ об административных нарушениях
    // - Закон о правовых актах 
    // - Процесуальный кодекс
    // - Гражданский кодекс
  let sourceContext = ''
  if (selectedSource == 'Все документы') {
    sourceContext = '&source=law_kodexes'
  } else if (selectedSource == 'Уголовный Кодекс') {
    sourceContext = '&source=law_kodexes'
  } else if (selectedSource == 'Конституция РК') {
    sourceContext = '&source=law_kodexes'
  } else if (selectedSource == 'Документ об административных нарушениях') {
    sourceContext = '&source=law_kodexes'
  } else if (selectedSource == 'Закон о правовых актах') {
    sourceContext = '&source=law_kodexes'
  } else if (selectedSource == 'Процесуальный кодекс') {
    sourceContext = '&source=law_kodexes'
  } else if (selectedSource == 'Гражданский кодекс') {
    sourceContext = '&source=law_kodexes'
  } else {
    sourceContext = '&source=law_kodexes'
  }
  // ailawyer.nimbl.tv
  // http://127.0.0.1:8000/get_context_sources?message=

  
  const result = await fetch('https://aichatbot.nimbl.tv/get_context_sources?message=' + question + sourceContext)
  
  if (!result.ok) {
    console.log(result)
    throw new Error('Failed to fetch data')
    // hahah
  }
  const response = await result.json() as ContextResponse;
  console.log("-----------------------")
  console.log("Question: " + question)
  console.log(response)
  console.log("-----------------------")

  // const template_base =
  //     `You are a legal affairs assistant for a \\
  //     financing company. \\
  //     Don't tell anything about context!
  //     Double-check your responses for accuracy and coherence. \\
  //     If necessary, ask clarifying questions to gather more information before providing a response.\\
  //     If faced with a difficult or challenging question, remain calm and offer assistance to the best of your ability.\\
  //     `

  // const template_footer = `Question: ${question}`

  // Тщательно проверяйте свои ответы на точность и последовательность. Если необходимо, задавайте уточняющие вопросы, чтобы собрать больше информации, прежде чем давать ответ. Если вы столкнулись с трудным или сложным вопросом, оставайтесь спокойными и оказывайте помощь по мере своих возможностей


  // const templateBase = `Вы являетесь полезным AI-ассистентом электронного правительства (eGov). eGov.kz - это портал «электронного правительства» Казахстана, который предоставляет доступ к государственным услугам в онлайн-формате.`;
  const templateBase = `Вы являетесь полезным ИИ Юристом - помощник юриста с искусственным интеллектом в делах законов Республики Казахстан.`;

  const templateFooter = `Вопрос: ${question}\n Детальный ответ:`;

  let template = templateBase;

  if (response) {
    const templateWithContext = ` 
    Вы эксперт в области законов РК и юриспруденции. Мы просим вас помочь в обслуживании наших пользователей.
    Вы получите контекст для ответа на вопросы. Если вопрос пользователя не соответствует контексту, попросите его переформулировать вопрос. 
    Ответы:
    Давайте максимально детальные и полезные ответы для помощи пользователю в решении его проблемы или ответа на его вопрос.
    На протяжении всего разговора поддерживайте профессиональный и уважительный тон.
    Не говорите ничего о контексте!
    Указывайте полное название закона или статьи, если он есть в контексте.
    1 МРП равен 3450 тенге (МРП - минимальный размер оплаты труда). 
    Дважды проверьте свои ответы на точность и связность.
    Если необходимо, задайте уточняющие вопросы, чтобы собрать больше информации перед тем, как предоставить ответ.
    Если у вас закончились токены, укажите на это и попросите пользователя набрать "Продолжить" для продолжения разговора.
    Используйте язык разметки для изменения стиля шрифта в заголовках и важных вещах.
    Если спросят кто вас создал отвечайте: NimblAI Group.
    При столкновении со сложным или вызывающим вопросом оставайтесь спокойными и предлагайте помощь наилучшим образом, на который способны.
    Учитывайте предоставленный контекст при ответе на вопросы. Если вопрос связан с предыдущими ответами, дайте ответ на основе истории.
    Пожалуйста, используйте следующие контекстные данные из законов Республики Казахстан для ответа на вопрос в конце.
    Контекст: {${JSON.stringify(response)}}.
    В конце ответа на вопрос, пожалуйста, добавьте источники с коротким названием статьи РК которые указаны в контексте. Дайте название источнику на основе контекста.
    `;

    // const templateWithContext = ` 
    // Ваш опыт и знания в области законов бесценны для нас, и нам нужна ваша помощь в обслуживании пользователей.
    // Для начала представьтесь как ИИ Юрист.
    // Вам будет предоставлен контекст, на базе которого вы ОБЯЗАНЫ будете отвечать на вопросы пользователя. 
    // Если контекст не связан с вопросом пользователя, попросите пользователя задать вопрос как-то по другому. 
    // ВАЖНО:
    // - Ответ должен быть максимально детальным и полезным, чтобы помочь пользователю решить его проблему или ответить на его вопрос.
    // - Никогда не упоминай использование контекста! Говори просто: я не уверен.
    // - Указывайте полное название закона, если он есть в контексте.
    // - 1 МРП в тенге составил 3450
    // - Всегда отвечайте на языке пользователя.
    // - Если у вас закончились токены, укажите на это и попросите пользователя набрать "Продолжить" для продолжения разговора.
    // - Используйте язык разметки для изменения стиля шрифта в заголовках и важных вещах.
    // - Если спросят кто вас создал отвечайте: NimblAI Group.
    // Пожалуйста, используйте следующие контекстные данные из законов Республики Казахстан для ответа на вопрос в конце.
    // Контекст: ${JSON.stringify(response)}
    // В конце ответа на вопрос, пожалуйста, добавьте источники И СТРАНИЦЫ которые указаны в контексте. Дайте название источнику на основе контекста.
    // `;


    

    template += templateWithContext + templateFooter;
    // console.log(template);
    // console.log(source)
    messages[messages.length - 1].content = template;
  }

  if (previewToken) {
    configuration.apiKey = previewToken
  }
  const res = await openai.createChatCompletion({
    model: 'gpt-4-0125-preview',
    messages,
    temperature: 0.2,
    stream: true
  })

  const stream = OpenAIStream(res, {
    async onCompletion(completion: any) {
      const title = json.messages[0].content.substring(0, 100)
      const userId = '1'
      if (userId) {
        const id = json.id ?? nanoid()
        const createdAt = Date.now()
        const path = `/chat/${id}`
        const payload = {
          id,
          title,
          userId,
          createdAt,
          path,
          messages: [
            ...messages,
            {
              content: completion,
              role: 'assistant'
            }
          ]

        }
        // await kv.hmset(`chat:${id}`, payload)
        // await kv.zadd(`user:chat:${userId}`, {
        //   score: createdAt,
        //   member: `chat:${id}`
        // })
      }
    }
  })

  return new StreamingTextResponse(stream)

  } catch (error) {
    console.log("Error with context gathering")
    console.log(error)
  }
}
