'use client'

import Image from 'next/image'
import { type Session } from 'next-auth'
import { signOut } from 'next-auth/react'

import { Button } from '@/components/ui/button'
import {
    Select,
    SelectGroup,
    SelectValue,
    SelectTrigger,
    SelectContent,
    SelectLabel,
    SelectItem,
    SelectSeparator
} from '@/components/ui/select'
import { IconExternalLink } from '@/components/ui/icons'
import { useState } from 'react'

export interface SelectLawMenuProps {
    selectedSource: string | null;
    setSelectedSource: (source: string | null) => void;
}

export function SelectLawMenu({selectedSource, setSelectedSource} : SelectLawMenuProps) {
    // const [selectedSource, setSelectedSource] = useState<string | null>(null);
    return (
        <Select
            // value={selectedFruit}
            onValueChange={setSelectedSource}
      >
        <SelectTrigger>
          {selectedSource || 'Выбрать источник'}
        </SelectTrigger>
  
        <SelectContent>
          <SelectItem value="Все документы">Все документы</SelectItem>
          <SelectItem value="Уголовный Кодекс">Уголовный Кодекс</SelectItem>
          <SelectItem value="Конституция РК">Конституция РК</SelectItem>
          {/* <SelectSeparator /> */}
          <SelectItem value="Документ об административных нарушениях">Документ об административных нарушениях</SelectItem>
          <SelectItem value="Закон о правовых актах">Закон о правовых актах</SelectItem>
          <SelectItem value="Процесуальный кодекс">Процесуальный кодекс</SelectItem>
          <SelectItem value="Гражданский кодекс">Гражданский кодекс</SelectItem>
        </SelectContent>
      </Select>
  )
}

// Все документы
// Уголовный Кодекс
// - Конституция РК
// - Документ об административных нарушениях
// - Закон о правовых актах 
// - Процесуальный кодекс
// - Гражданский кодекс